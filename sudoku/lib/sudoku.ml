
type vertex = int*int 
type gamma = int

module Vertex = Map.Make(struct
    type t = vertex
    let compare = Stdlib.compare
  end)

module Gamma = Set.Make(struct
    type t = gamma
    let compare = Stdlib.compare
  end)

type game_map = Gamma.t Vertex.t

let make_vertex x y = (x, y) 

let make_gamma x = x 

let free_gamma = 
  let gamma = ref Gamma.empty in
  for x = 1 to 9 do
    gamma := ! gamma |> Gamma.add x
  done;
  !gamma

let print_gamma g = Gamma.iter print_int g

let free_gamma_vertex = 
  let vertex = ref Vertex.empty in 
  for x = 0 to 8 do
    for y = 0 to 8 do
      vertex := ! vertex |> Vertex.add (x, y) free_gamma
    done 
  done;
  !vertex

let print_vertex v = 
  let print_node (x,y) g = 
    Printf.printf "\n(%d, %d) = " x y;
    print_gamma g
  in
  Vertex.iter print_node v

let print_pretty_sudoku g =
  match g with
  | Some g ->
    let print_node (_x,_y) _g =
      if _y = 0 
      then Printf.printf "\n | ";
      print_gamma _g;
      print_string " | "
    in
    Vertex.iter print_node g
  | None -> print_string "OOps"


(** [fold_vertices f] applicerar [f v] över alla hörn i sudokugrafen *)
let fold_vertices f game = Vertex.fold f game game

let fold_neighbours (neigh : vertex) (f : (vertex -> Gamma.t -> 'a -> 'a)) game =
  let in_quad (x1, y1) (x2, y2) = 
    x1/3 = x2/3 && y1/3 = y2/3 
  in 
  let is_neighbour v n = 
    in_quad v n || fst v = fst n || snd v = snd n
  in 
  let filter (v' : vertex) (g' : Gamma.t) (a : 'a) : 'a = 
    if is_neighbour neigh v' then
      f v' g' a
    else 
      a
  in
  fold_vertices filter game

let fold_candidates neigh f acc = 
  let filter _v g a = f g a
  in
  fold_neighbours neigh filter acc


let apply (v : vertex) (g : gamma) (game : game_map) : game_map option =
  let  set_g ver gam game =
    let gam' = Gamma.empty |> Gamma.add gam 
    in
    Vertex.remove ver game |> Vertex.add ver gam'
  in
  let remove_g ver gam game =
    let gam' = Gamma.remove g gam
    in
    Vertex.remove ver game |> Vertex.add ver gam'
  in
  Some (fold_neighbours v remove_g game |> set_g v g)


(** [color pc] använder backtracking för att ge sudokugrafen en
    komplett färgläggning kompatibel med [pc]. *)

let color _game = None

(** [ncolor pc] använder backtracking för att beräkna antalet
    färgläggningar som är kompatibla med [pc]. *)
let ncolor _pc = 0


let rec resolve (v : vertex) (g : game_map) : game_map option =
  let alt_list = (Vertex.find v g)
  in
  let is_last_vertex (x, y) = x = 8 && y = 8
  in
  let next_vertex (x, y) =
    let tal = x * 9 + y + 1
    in 
    (tal/9 , tal mod 9)
  in
  let check_vertex_game v game gamma res =
    if Option.is_some res then res
    else 
      let game' = apply v gamma game
      in
      match game' with
      | Some game -> 
        if is_last_vertex v then Some game
        else resolve (next_vertex v) game
      | None -> None
  in 
  Gamma.fold (check_vertex_game v g) alt_list None

let init_game lst =
  let game = ref free_gamma_vertex
  in 
  for x = 0 to 80 do
    if (List.nth lst x) = 0 then game := !game
    else 
      game := !game |> apply (x/9, x mod 9) (List.nth lst x) |> Option.get
  done;
  !game