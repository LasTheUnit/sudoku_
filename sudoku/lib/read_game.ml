let process_number tal acc = (tal :: acc)

let rec process_rows stream acc =
  match Stream.peek stream with
  | None -> acc
  | Some _ -> process_number (Stream.next stream) acc |> process_rows stream
(*   processa_rader stream (processa_raden (Stream.next stream) acc) *)

let read filename =
  let infile = Scanf.Scanning.open_in filename in
  let callback infile number =
    if number >= 81 then None 
    else
      (Scanf.bscanf infile " %d" (fun i -> Some i))
  in
  let stom = Stream.from (callback infile) in
  try
    let list = process_rows stom []
    in
    Scanf.Scanning.close_in infile;
    List.rev list
  with e ->
    Scanf.Scanning.close_in infile;
    raise e;