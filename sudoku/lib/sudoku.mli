type vertex (* Hörn: Celler i sudoku *)
type gamma (* Färger:  1 - 9 *)

(* [G = Set.Make(Gamma)] *)
module Gamma: Set.S with type elt = gamma (*  Set.Make(Gamma) *)
(* [V = Map.Make(Vertex)] *)
module Vertex: Map.S with type key = vertex (*  Map.Make(Vertex) *)


type game_map = Gamma.t Vertex.t

val make_vertex: int -> int -> vertex

val make_gamma: int -> gamma

val print_gamma: Gamma.t -> unit

val free_gamma: Gamma.t

val print_vertex: game_map -> unit

val print_pretty_sudoku: game_map option -> unit

val free_gamma_vertex: game_map

val apply: vertex -> gamma -> game_map -> game_map option


(** [fold_neighbours v f a] applicerar [f w] över alla grannar [w] till
    [v] i sudokugrafen *)
val fold_neighbours: vertex -> (vertex -> Gamma.t -> game_map -> game_map) -> game_map -> game_map

(** [fold_candidates v f a] applicerar [f g] över alla kandidater [w.g] där
    alla [w] är grannar till [v] i sudokugrafen *)
val fold_candidates: vertex -> (Gamma.t -> game_map -> game_map) -> game_map -> game_map

(** [fold_vertices f a] applicerar [f v] över alla hörn i sudokugrafen *)
val fold_vertices: (vertex -> Gamma.t -> game_map -> game_map) -> game_map -> game_map 

(** [color pc] använder backtracking för att ge sudokugrafen en
    komplett färgläggning kompatibel med [pc]. *)
val color: game_map -> game_map option

(** [ncolor pc] använder backtracking för att beräkna antalet
    färgläggningar som är kompatibla med [pc]. *)
val ncolor: game_map -> int

val resolve: vertex -> game_map -> game_map option

val init_game: int list -> game_map 